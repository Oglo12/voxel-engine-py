from ursina import Vec3, color
# -------- SETTINGS --------

# Face
FACE_DOUBLE_SIDED = False
FACE_DEBUG_COLORING = False

# --------------------------

DIRECTIONS = ["N", "S", "E", "W", "U", "D"]

AXIS_MOD = {
    DIRECTIONS[0]: Vec3(0, 0, 1),
    DIRECTIONS[1]: Vec3(0, 0, -1),
    DIRECTIONS[2]: Vec3(1, 0, 0),
    DIRECTIONS[3]: Vec3(-1, 0, 0),
    DIRECTIONS[4]: Vec3(0, 1, 0),
    DIRECTIONS[5]: Vec3(0, -1, 0),
}

FACE_ROTATIONS = {
    DIRECTIONS[0]: Vec3(0, -180, 0),
    DIRECTIONS[1]: Vec3(0, 0, 0),
    DIRECTIONS[2]: Vec3(0, -90, 0),
    DIRECTIONS[3]: Vec3(0, 90, 0),
    DIRECTIONS[4]: Vec3(90, 0, 0),
    DIRECTIONS[5]: Vec3(-90, 0, 0),
}

FACE_DEBUG_COLORS = {
    DIRECTIONS[0]: color.rgb(255, 0, 0),
    DIRECTIONS[1]: color.rgb(255, 155, 0),
    DIRECTIONS[2]: color.rgb(255, 255, 0),
    DIRECTIONS[3]: color.rgb(0, 255, 0),
    DIRECTIONS[4]: color.rgb(0, 0, 255),
    DIRECTIONS[5]: color.rgb(255, 0, 255),
}
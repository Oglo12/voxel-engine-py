from ursina import *

from config import *
from texture import texture
from data import Data

def type_info(type_id):
    info = VoxelMisc.ids.get(type_id)

    if info != None:
        return info
    else:
        return VoxelMisc.ids.get("debug")

def get_info(type_id, info):
    loaded = type_info(type_id)
    value = loaded.get(info)

    if value != None:
        return value
    else:
        return VoxelMisc.id_info_defaults.get(type_id).get(info)

class VoxelMisc:
    ids = {
        "debug": {
            "N": texture("b:debug"),
            "S": texture("b:debug"),
            "E": texture("b:debug"),
            "W": texture("b:debug"),
            "U": texture("b:debug"),
            "D": texture("b:debug"),
            "light": 16,
        },

        "air": {
            "density": 0,
            "skip": True,
        },
    }

    id_info_defaults = {
        "N": texture("null"), # North texture.
        "S": texture("null"), # South texture.
        "E": texture("null"), # East texture.
        "W": texture("null"), # West texture.
        "U": texture("null"), # Up texture.
        "D": texture("null"), # Down texture.
        "density": 1, # (0 => 1) 0: Pass right through, 1: Completely solid.
        "skip": False, # Should this voxel be completely overlooked?
        "light_amount": 0, # (0 => 1) The amount of light the voxel makes.
        "light_color": Vec3(255, 255, 255), # (Vec3(0, 0, 0) => Vec3(255, 255, 255)) The color of the light the block makes.
        "heat": 0, # (-Infinite => Infinite) -: Cold, +: Hot.
        "melt_temp": None, # (-Infinite => Infinite OR None) The temp where the block breaks (or melts). None: Doesn't melt at all.
        "bounce": 0.2, # (0 => Infinite) The amount of blocks the block can bounce.
        "weight": 100, # (0 => Infinite) The weight of the block in pounds.
    }

    voxels = {}

class Voxel(Entity):
    def __init__(self, position = (0, 0, 0), type_id = "debug"):
        super().__init__(
            model = None,
            texture = None,
            color = None,
            position = position,
            type_id = type_id,

            spawn_ran = False,
        )
    
    def update(self):
        if self.spawn_ran == False:
            self.hard_when_spawned()
            self.when_spawned()
            self.spawn_ran = True
        
        self.hard_loop()
        self.loop()
    
    def hard_when_spawned(self):
        self.faces = {}

        self.add_voxel()

        for i in DIRECTIONS:
            self.make_face(i)

    def hard_loop(self):
        pass
    
    def when_spawned(self):
        pass

    def loop(self):
        pass

    def add_voxel(self):
        VoxelMisc.voxels[self.position] = self
        Data.blocks[self.position] = self.type_id
    
    def remove_voxel(self):
        VoxelMisc.voxels[self.position] = None
        Data.blocks[self.position] = self.type_id

    def make_face(self, axis):
        self.faces[axis] = VoxelFace(parent = self, axis = axis, type_id = self.type_id)

class VoxelFace(Entity):
    def __init__(self, parent = None, axis = None, type_id = "debug", used = True):
        super().__init__(
            parent = parent,
            model = "quad",
            color = color.rgb(255, 255, 255),
            texture = texture("null"),
            type_id = type_id,
            axis = axis,
            origin = (0, 0, 0.5),
            double_sided = FACE_DOUBLE_SIDED,

            spawn_ran = False,
        )
    
    def update(self):
        if self.spawn_ran == False:
            self.hard_when_spawned()
            self.when_spawned()
            self.spawn_ran = True
        
        self.hard_loop()
        self.loop()
    
    def hard_when_spawned(self):
        self.rotation = FACE_ROTATIONS.get(self.axis)
        self.texture = get_info(self.type_id, self.axis)

        if FACE_DEBUG_COLORING == True:
            self.color = FACE_DEBUG_COLORS.get(self.axis)

    def hard_loop(self):
        if self.is_side() == True:
            self.enable_face()
        else:
            self.disable_face()

    def when_spawned(self):
        pass

    def loop(self):
        pass

    def enable_face(self):
        self.visible = True

    def disable_face(self):
        self.visible = False

    def side_position(self):
        return self.parent.position + AXIS_MOD.get(self.axis)
    
    def is_side(self):
        side = Data.blocks.get(self.side_position())

        if side == None:
            return True

def set_block(position, type_id):
    erase_block(position)
    return Voxel(position = Vec3(position[0], position[1], position[2]), type_id = type_id)

def erase_block(position):
    block = VoxelMisc.voxels.get(position)

    if block == None:
        return

    Voxel.remove_voxel(block)
    destroy(block)
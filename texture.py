class Textures:
    textures = {
        "null": "white_cube",

        "b:debug": "white_cube",
    }

def texture(type_id):
    return Textures.textures.get(type_id)
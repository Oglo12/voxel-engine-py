#################
#   Main File   #
#################

"""

---- TODO ----
1. Disable dev mode.
2. Add lighting.
3. Explore using Numba to make code faster!
4. Make chunk loading.
5. Use perlin noise.
6. Finish adding block tag functionality. [ density, skip, light_amount, light_color, heat, melt_temp, bounce, weight ]
7. Move blocktypes to a separate file.
8. Make faces dynamically load.
--------------

"""

from ursina import *
from numpy import floor

from voxel import *

window.borderless = False

app = Ursina()

for i in range(0, 20 * 20):
    set_block((floor(i / 20), 0, floor(i % 20)), "debug")

EditorCamera()

app.run()